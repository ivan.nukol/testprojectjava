package com.example.quwi.db.converters;

import androidx.room.TypeConverter;

import com.example.quwi.net.model.response.channels.MessageLast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class MessageLastConverters {
    private MessageLastConverters() {
    }

    @TypeConverter
    public static String ObjectToString(MessageLast value) {
        return value == null ? null : new Gson().toJson(value);
    }

    @TypeConverter
    public static MessageLast stringToObject(String value) {
        Type listType = new TypeToken<MessageLast>() {
        }.getType();
        return value == null ? null : new Gson().fromJson(value, listType);
    }
}
