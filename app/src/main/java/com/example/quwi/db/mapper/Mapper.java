package com.example.quwi.db.mapper;

import java.util.List;

public interface Mapper<T, F> {
    T mapTo(F item);

    F mapFrom(T item);

    List<T> mapTo(List<F> items);

    List<F> mapFrom(List<T> items);
}