package com.example.quwi.db;


import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.quwi.db.converters.DraftConverters;
import com.example.quwi.db.converters.ListLongConverters;
import com.example.quwi.db.converters.MessageLastConverters;
import com.example.quwi.db.dao.ChannelsDao;
import com.example.quwi.db.entity.ChannelsEntity;

@Database(entities = {ChannelsEntity.class,}, version = 1)

@TypeConverters({DraftConverters.class, ListLongConverters.class, MessageLastConverters.class})
public abstract class AppDatabase extends RoomDatabase {
    public static final String NAME_BD = "quwi.db";

    public abstract ChannelsDao getChannelsDao();


}