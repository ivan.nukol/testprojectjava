package com.example.quwi.db.dao;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.quwi.db.entity.ChannelsEntity;

import java.util.List;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;

@Dao
public interface ChannelsDao {
    @Query("SELECT * FROM channels")
    Flowable<List<ChannelsEntity>> getAll();

    @Insert(onConflict = REPLACE)
    Single<Long> insert(ChannelsEntity channels);

    @Insert(onConflict = REPLACE)
    Single<List<Long>> insert(List<ChannelsEntity> memberEntities);

    @Query("DELETE FROM channels")
    Integer deleteAll();

    @Query("DELETE FROM channels")
    Single<Integer> clareDate();
}
