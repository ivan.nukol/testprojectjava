package com.example.quwi.db.mapper;

import com.example.quwi.db.entity.ChannelsEntity;
import com.example.quwi.net.model.response.channels.Channel;
import com.example.quwi.net.model.response.channels.Channels;

public class ChannelsMapper extends MapperList<ChannelsEntity, Channel> {
    @Override
    public ChannelsEntity mapTo(Channel item) {
        return new ChannelsEntity(
                item.getId(),
                item.getDraft(),
                item.getDtaChangeMsg(),
                item.getDtaCreate(),
                item.getDtaLastRead(),
                item.getIdPartner(),
                item.getIdUsers(),
                item.getIsHideInChatsList(),
                item.getIsMute(),
                item.getIsStarred(),
                item.getIsUnreadManual(),
                item.getMessageLast(),
                item.getMuteUntilPeriod(),
                item.getPinToTop(),
                item.getType()
        );
    }

    @Override
    public Channel mapFrom(ChannelsEntity item) {
        return new Channel(
                item.getId(),
                item.getDraft(),
                item.getDtaChangeMsg(),
                item.getDtaCreate(),
                item.getDtaLastRead(),
                item.getIdPartner(),
                item.getIdUsers(),
                item.getIsHideInChatsList(),
                item.getIsMute(),
                item.getIsStarred(),
                item.getIsUnreadManual(),
                item.getMessageLast(),
                item.getMuteUntilPeriod(),
                item.getPinToTop(),
                item.getType()
        );
    }
}
