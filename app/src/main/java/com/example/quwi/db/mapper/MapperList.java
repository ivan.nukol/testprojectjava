package com.example.quwi.db.mapper;

import java.util.ArrayList;
import java.util.List;

public abstract class MapperList<T, F> implements Mapper<T, F> {
    @Override
    public List<T> mapTo(List<F> items) {
        List<T> memberEntities = new ArrayList<>();
        for (F item : items) {
            memberEntities.add(mapTo(item));
        }
        return memberEntities;
    }

    @Override
    public List<F> mapFrom(List<T> items) {
        List<F> member = new ArrayList<>();
        for (T item : items) {
            member.add(mapFrom(item));
        }
        return member;
    }
}
