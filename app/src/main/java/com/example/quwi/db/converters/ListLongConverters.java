package com.example.quwi.db.converters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ListLongConverters {
    ListLongConverters() {
    }
    @TypeConverter
    public static String ObjectToString(List<Long> value) {
        return value == null ? null : new Gson().toJson(value);
    }

    @TypeConverter
    public static List<Long> stringToObject(String value) {
        Type listType = new TypeToken<List<Long>>() {
        }.getType();
        return value == null ? null : new Gson().fromJson(value, listType);
    }

}
