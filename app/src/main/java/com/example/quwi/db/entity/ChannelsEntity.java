package com.example.quwi.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.quwi.net.model.response.channels.Draft;
import com.example.quwi.net.model.response.channels.MessageLast;

import java.util.List;

@Entity(tableName = "channels")
public class ChannelsEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_primary")
    private Long idPrimary;
    @ColumnInfo(name = "id")
    private Long id;
    @ColumnInfo(name = "draft")
    private Draft mDraft;
    @ColumnInfo(name = "dta_change_msg")
    private String mDtaChangeMsg;
    @ColumnInfo(name = "dta_create")
    private String mDtaCreate;
    @ColumnInfo(name = "dta_last_read")
    private String mDtaLastRead;
    @ColumnInfo(name = "id_partner")
    private Long mIdPartner;
    @ColumnInfo(name = "id_users")
    private List<Long> mIdUsers;
    @ColumnInfo(name = "is_hide_in_chats_list")
    private Boolean mIsHideInChatsList;
    @ColumnInfo(name = "is_mute")
    private Boolean mIsMute;
    @ColumnInfo(name = "is_starred")
    private Boolean mIsStarred;
    @ColumnInfo(name = "is_unread_manual")
    private Boolean mIsUnreadManual;
    @ColumnInfo(name = "message_last")
    private MessageLast mMessageLast;
    @ColumnInfo(name = "mute_until_period")
    private Long mMuteUntilPeriod;
    @ColumnInfo(name = "pin_to_top")
    private Boolean mPinToTop;
    @ColumnInfo(name = "type")
    private String mType;

    public ChannelsEntity(Long id, Draft mDraft, String mDtaChangeMsg, String mDtaCreate, String mDtaLastRead, Long mIdPartner, List<Long> mIdUsers, Boolean mIsHideInChatsList, Boolean mIsMute, Boolean mIsStarred, Boolean mIsUnreadManual, MessageLast mMessageLast, Long mMuteUntilPeriod, Boolean mPinToTop, String mType) {
        this.id = id;
        this.mDraft = mDraft;
        this.mDtaChangeMsg = mDtaChangeMsg;
        this.mDtaCreate = mDtaCreate;
        this.mDtaLastRead = mDtaLastRead;
        this.mIdPartner = mIdPartner;
        this.mIdUsers = mIdUsers;
        this.mIsHideInChatsList = mIsHideInChatsList;
        this.mIsMute = mIsMute;
        this.mIsStarred = mIsStarred;
        this.mIsUnreadManual = mIsUnreadManual;
        this.mMessageLast = mMessageLast;
        this.mMuteUntilPeriod = mMuteUntilPeriod;
        this.mPinToTop = mPinToTop;
        this.mType = mType;
    }

    public Long getIdPrimary() {
        return idPrimary;
    }

    public void setIdPrimary(Long idPrimary) {
        this.idPrimary = idPrimary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Draft getDraft() {
        return mDraft;
    }

    public void setDraft(Draft mDraft) {
        this.mDraft = mDraft;
    }

    public String getDtaChangeMsg() {
        return mDtaChangeMsg;
    }

    public void setDtaChangeMsg(String mDtaChangeMsg) {
        this.mDtaChangeMsg = mDtaChangeMsg;
    }

    public String getDtaCreate() {
        return mDtaCreate;
    }

    public void setDtaCreate(String mDtaCreate) {
        this.mDtaCreate = mDtaCreate;
    }

    public String getDtaLastRead() {
        return mDtaLastRead;
    }

    public void setDtaLastRead(String mDtaLastRead) {
        this.mDtaLastRead = mDtaLastRead;
    }


    public Long getIdPartner() {
        return mIdPartner;
    }

    public void setIdPartner(Long mIdPartner) {
        this.mIdPartner = mIdPartner;
    }

    public List<Long> getIdUsers() {
        return mIdUsers;
    }

    public void setIdUsers(List<Long> mIdUsers) {
        this.mIdUsers = mIdUsers;
    }

    public Boolean getIsHideInChatsList() {
        return mIsHideInChatsList;
    }

    public void setIsHideInChatsList(Boolean mIsHideInChatsList) {
        this.mIsHideInChatsList = mIsHideInChatsList;
    }

    public Boolean getIsMute() {
        return mIsMute;
    }

    public void setIsMute(Boolean mIsMute) {
        this.mIsMute = mIsMute;
    }

    public Boolean getIsStarred() {
        return mIsStarred;
    }

    public void setIsStarred(Boolean mIsStarred) {
        this.mIsStarred = mIsStarred;
    }

    public Boolean getIsUnreadManual() {
        return mIsUnreadManual;
    }

    public void setIsUnreadManual(Boolean mIsUnreadManual) {
        this.mIsUnreadManual = mIsUnreadManual;
    }

    public MessageLast getMessageLast() {
        return mMessageLast;
    }

    public void setMessageLast(MessageLast mMessageLast) {
        this.mMessageLast = mMessageLast;
    }

    public Long getMuteUntilPeriod() {
        return mMuteUntilPeriod;
    }

    public void setMuteUntilPeriod(Long mMuteUntilPeriod) {
        this.mMuteUntilPeriod = mMuteUntilPeriod;
    }

    public Boolean getPinToTop() {
        return mPinToTop;
    }

    public void setPinToTop(Boolean mPinToTop) {
        this.mPinToTop = mPinToTop;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }
}
