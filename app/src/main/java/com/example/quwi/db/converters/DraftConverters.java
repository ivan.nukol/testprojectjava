package com.example.quwi.db.converters;

import androidx.room.TypeConverter;

import com.example.quwi.net.model.response.channels.Draft;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class DraftConverters {

    private DraftConverters() {
    }

    @TypeConverter
    public static String ObjectToString(Draft value) {
        return value == null ? null : new Gson().toJson(value);
    }

    @TypeConverter
    public static Draft stringToObject(String value) {
        Type listType = new TypeToken<Draft>() {
        }.getType();
        return value == null ? null : new Gson().fromJson(value, listType);
    }
}
