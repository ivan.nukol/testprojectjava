package com.example.quwi.preference;

import static com.example.quwi.preference.PrefEntity.PREFERENCES_FILE_NAME;

import android.content.Context;

public class SharedPreferencesData {
    Context context;

    public SharedPreferencesData(Context context) {
        this.context = context;
    }

    private android.content.SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void setPreferenceString(String key, String value) {
        android.content.SharedPreferences settings = getSharedPreferences();
        android.content.SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setPreferenceInt(String key, int value) {
        android.content.SharedPreferences settings = getSharedPreferences();
        android.content.SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public String getPreferenceString(String key) {
        return getSharedPreferences().getString(key, "");
    }

    public boolean getPreferenceBoolean(String key, boolean returnParameter) {
        return getSharedPreferences().getBoolean(key, returnParameter);
    }

    public int getPreferenceInt(String key) {
        return getSharedPreferences().getInt(key, 1);
    }

    public  void clareDate(String key) {
        getSharedPreferences() .edit().remove(key).apply();
    }
}
