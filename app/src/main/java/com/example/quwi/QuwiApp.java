package com.example.quwi;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;

import com.example.quwi.di.AppComponent;
import com.example.quwi.di.AppInjector;

import java.util.HashMap;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class QuwiApp extends Application implements HasAndroidInjector {

    private static QuwiApp instance;
    private static AppComponent appComponent;

    @Inject
    DispatchingAndroidInjector<Object> dispatchingAndroidInjector;

    @Override
    public AndroidInjector<Object> androidInjector() {
        return dispatchingAndroidInjector;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appComponent = AppInjector.init(this);
    }

    public static QuwiApp getInstance() {
        return instance;
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
