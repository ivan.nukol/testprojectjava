package com.example.quwi.repository;


import android.util.Log;

import com.example.quwi.net.error.ErrorModel;
import com.example.quwi.net.error.NetworkException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import com.google.gson.Gson;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;
import retrofit2.HttpException;

import java.io.IOException;
import java.net.UnknownHostException;

public abstract class BaseRepository {
    public <T> Single<T> bindWithUtils(Single<T> observable) {
        return observable.onErrorResumeNext(new Function<Throwable, SingleSource<? extends T>>() {
            @Override
            public SingleSource<? extends T> apply(Throwable throwable) throws Throwable {
                if (throwable instanceof HttpException) {
                    try {
                        JsonObject jsonObject =
                                new JsonParser().parse(((HttpException) throwable).response().errorBody().string()).getAsJsonObject();
                        Gson gson = new Gson();
                        NetworkException error =
                                new NetworkException(gson.fromJson(jsonObject, ErrorModel.class), null);
                        return Single.error(error);
                    } catch (IOException ignored) {
                    } catch (Exception e) {
                        Log.e("", "error while parse server error message");
                        return Single.error(new NetworkException(null, throwable)
                        );
                    }
                }
                return Single.error(throwable);
            }
        });
    }
}
