package com.example.quwi.repository.authorization;

import com.example.quwi.net.api.AuthorizationApi;
import com.example.quwi.net.model.request.LoginBody;
import com.example.quwi.net.model.request.RegistrationBody;
import com.example.quwi.net.model.response.login.LoginResponse;
import com.example.quwi.preference.PrefEntity;
import com.example.quwi.preference.SharedPreferencesData;
import com.example.quwi.repository.BaseRepository;

import javax.inject.Inject;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AuthorizationRepositoryImpl extends BaseRepository implements AuthorizationRepository {
    private final AuthorizationApi authorizationApi;
    private final SharedPreferencesData sharedPreferencesData;

    @Inject
    AuthorizationRepositoryImpl(AuthorizationApi authorizationApi, SharedPreferencesData sharedPreferencesData) {
        this.authorizationApi = authorizationApi;
        this.sharedPreferencesData = sharedPreferencesData;
    }

    @Override
    public Single<LoginResponse> login(LoginBody loginBody) {
        return bindWithUtils(authorizationApi.login(loginBody))
                .subscribeOn(Schedulers.io())
                .doOnSuccess(loginResponse -> sharedPreferencesData.setPreferenceString(PrefEntity.TOKEN, loginResponse.getToken()));
    }

    @Override
    public Single<LoginResponse> registration(RegistrationBody registrationBody) {
        return bindWithUtils(authorizationApi.registration(registrationBody))
                .subscribeOn(Schedulers.io())
                .doOnSuccess(loginResponse -> sharedPreferencesData.setPreferenceString(PrefEntity.TOKEN, loginResponse.getToken()));
    }
}
