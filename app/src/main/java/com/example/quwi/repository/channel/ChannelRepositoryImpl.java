package com.example.quwi.repository.channel;

import android.util.Log;

import com.example.quwi.db.dao.ChannelsDao;
import com.example.quwi.db.mapper.ChannelsMapper;
import com.example.quwi.net.api.ChannelApi;
import com.example.quwi.net.model.response.channels.Channel;
import com.example.quwi.net.model.response.channels.Channels;
import com.example.quwi.repository.BaseRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ChannelRepositoryImpl extends BaseRepository implements ChannelRepository {
    private static final String BEARER = "Bearer %s";

    private final ChannelApi channelApi;
    private final ChannelsDao channelsDao;
    private final ChannelsMapper channelsMapper = new ChannelsMapper();

    @Inject
    ChannelRepositoryImpl(ChannelApi channelApi, ChannelsDao channelsDao) {
        this.channelApi = channelApi;
        this.channelsDao = channelsDao;
    }

    @Override
    public Single<Channels> getChannels(String token) {
        return bindWithUtils(channelApi.getChannels(String.format(BEARER, token)))
                .subscribeOn(Schedulers.io())
                .doOnSuccess(channels -> {
                            channelsDao.deleteAll();
                            channelsDao.insert(channelsMapper.mapTo(channels.getChannels()))
                                    .subscribeOn(Schedulers.io())
                                    .subscribe();
                        }
                );
    }

    @Override
    public @NonNull Flowable<List<Channel>> subscribeChannels() {
        return channelsDao.getAll().map(channelsEntities -> channelsMapper.mapFrom(channelsEntities));
    }

    @Override
    public void clareChannelDB() {
        channelsDao.clareDate().subscribeOn(Schedulers.io())
                .subscribe();
    }
}
