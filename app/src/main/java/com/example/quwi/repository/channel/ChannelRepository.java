package com.example.quwi.repository.channel;

import com.example.quwi.net.model.response.channels.Channel;
import com.example.quwi.net.model.response.channels.Channels;

import java.util.List;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;
import retrofit2.http.Header;

public interface ChannelRepository {
    Single<Channels> getChannels(@Header("Authorization") String token);

    @NonNull Flowable<List<Channel>> subscribeChannels();

    void clareChannelDB();
}
