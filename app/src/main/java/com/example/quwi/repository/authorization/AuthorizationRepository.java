package com.example.quwi.repository.authorization;

import com.example.quwi.net.model.request.LoginBody;
import com.example.quwi.net.model.request.RegistrationBody;
import com.example.quwi.net.model.response.login.LoginResponse;

import io.reactivex.rxjava3.core.Single;

public interface AuthorizationRepository {

    Single<LoginResponse> login(LoginBody loginBody);

    Single<LoginResponse> registration(RegistrationBody RegistrationBody);
}
