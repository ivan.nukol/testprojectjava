package com.example.quwi.base;

import static androidx.navigation.Navigation.findNavController;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;


import com.example.quwi.di.fragment.Injectable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

import javax.inject.Inject;

public abstract class BaseFragment<T extends ViewDataBinding, V extends BaseViewModel> extends Fragment implements Injectable {
    protected T binding;
    protected V viewModel;

    @Inject
    public ViewModelProvider.Factory viewModelFactory;

    private Class<T> getBinding() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private Class<V> getViewModel() {
        return (Class<V>) getGenericSuperclass().getActualTypeArguments()[1];
    }

    private ParameterizedType getGenericSuperclass() {
        return (ParameterizedType) getClass().getGenericSuperclass();
    }

    public void setViewModel() {
        try {
            viewModel = new ViewModelProvider(this, viewModelFactory).get(getViewModel());
        } catch (Exception e) {
            viewModel = null;
        }
    }

    private void setBinding(@NonNull LayoutInflater inflater,
                            @Nullable ViewGroup container) {
        Method method = null;
        try {
            method = getBinding().getMethod("inflate", LayoutInflater.class, ViewGroup.class, boolean.class);
            Object result = method.invoke(null, new Object[]{inflater, container, false});
            binding = (T) result;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setBinding(inflater, container);
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setViewModel();
        setupLiveData();
        binding.executePendingBindings();
    }

    private void setupLiveData() {
        viewModel.getError().observe(getViewLifecycleOwner(), this::showMessage);
        viewModel.getErrorRes().observe(getViewLifecycleOwner(), this::showMessage);
    }

    protected void showMessage(String message) {
        if (message != null) {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    protected void showMessage(@StringRes int resStringId) {
        Toast.makeText(getContext(), resStringId, Toast.LENGTH_SHORT).show();
    }

    protected void navigationMain(NavDirections directions) {
        try {
            findNavController(binding.getRoot()).navigate(directions);
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
    }

    protected void back() {
        try {
            Navigation.findNavController(binding.getRoot()).popBackStack();
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
    }

}
