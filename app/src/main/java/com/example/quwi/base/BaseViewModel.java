package com.example.quwi.base;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.quwi.R;

import java.net.UnknownHostException;

import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;


public abstract class BaseViewModel extends ViewModel {

    private final MutableLiveData<String> error = new MutableLiveData<>();
    private final MutableLiveData<Integer> errorRes = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loader = new MutableLiveData<>();
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();


    public void checkError(Throwable throwable) {
        if (throwable instanceof UnknownHostException) {
            errorRes.postValue(R.string.there_is_no_internet_connection);
        } else {
            error.postValue(throwable.getMessage());
        }
    }

    public LiveData<String> getError() {
        return error;
    }

    public LiveData<Integer> getErrorRes() {
        return errorRes;
    }

    public void setLoader(Boolean isLoad) {
        loader.postValue(isLoad);
    }

    public LiveData<Boolean> getLoader() {
        return loader;
    }

    public void setThrowableError(String message) {
        Log.e(getClass().getName(), message);
    }

    @Override
    public void onCleared() {
        compositeDisposable.clear();
        super.onCleared();
    }

    protected void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    protected void removeDisposable(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
            compositeDisposable.remove(disposable);
        }
    }


}