package com.example.quwi.base;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.quwi.di.fragment.Injectable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

import javax.inject.Inject;

public abstract class BaseDialogFragment<B extends ViewDataBinding, V extends ViewModel> extends DialogFragment implements Injectable {
    protected B binding;
    protected V viewModel;

    @Inject
    public ViewModelProvider.Factory viewModelFactory;

    private Class<B> getBinding() {
        return (Class<B>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public Class<V> getViewModel() {
        return (Class<V>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    private void setBinding(@NonNull LayoutInflater inflater,
                            @Nullable ViewGroup container) {
        Method method = null;
        try {
            method = getBinding().getMethod("inflate", LayoutInflater.class, ViewGroup.class, boolean.class);
            Object result = method.invoke(null, new Object[]{inflater, container, false});
            binding = (B) result;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void setViewModel() {
        try {
            viewModel = new ViewModelProvider(this, viewModelFactory).get(getViewModel());
        } catch (Exception e) {
            viewModel = null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setBinding(inflater, container);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setViewModel();
        binding.executePendingBindings();
    }

    protected void showError(String message) {
        if (message != null) {
            showMessage(message);
        }
    }

    protected void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    protected void showMessage(@StringRes int resStringId) {
        Toast.makeText(getContext(), resStringId, Toast.LENGTH_SHORT).show();
    }

}
