package com.example.quwi.utils;

import android.widget.ImageView;

import androidx.annotation.DrawableRes;

import com.bumptech.glide.Glide;

public class GlideUtils {

    public static void glideSetImgForAvatar(ImageView iv, String url, @DrawableRes int id) {
        Glide.with(iv.getContext())
                .load(url)
                .circleCrop()
                .placeholder(id)
                .into(iv);
    }
}
