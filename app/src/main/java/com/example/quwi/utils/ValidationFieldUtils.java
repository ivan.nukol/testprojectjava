package com.example.quwi.utils;

import android.text.Editable;
import android.text.TextWatcher;

public class ValidationFieldUtils implements TextWatcher {
    private AfterTextChanged afterTextChanged;
    private OnTextChanged onTextChanged;
    private BeforeTextChanged beforeTextChanged;

    public ValidationFieldUtils(AfterTextChanged afterTextChanged, OnTextChanged onTextChanged, BeforeTextChanged beforeTextChanged) {
        this.afterTextChanged = afterTextChanged;
        this.onTextChanged = onTextChanged;
        this.beforeTextChanged = beforeTextChanged;
    }

    public ValidationFieldUtils(AfterTextChanged afterTextChanged) {
        this.afterTextChanged = afterTextChanged;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        if (beforeTextChanged != null)
            beforeTextChanged.change(s, start, count, after);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (onTextChanged != null)
            onTextChanged.change(s, start, before, count);
    }

    @Override
    public void afterTextChanged(Editable s) {
        afterTextChanged.change(s);
    }

    public interface AfterTextChanged {
        void change(Editable s);
    }

    public interface OnTextChanged {
        void change(CharSequence s, int start, int before, int count);
    }

    public interface BeforeTextChanged {
        void change(CharSequence s, int start, int count, int after);
    }

}
