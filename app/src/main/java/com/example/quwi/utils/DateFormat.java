package com.example.quwi.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormat {
    private static final String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
    private static final String EEE_HH_MM = "EEE HH:mm";

    public static String getTime(String nextClassDate) {
        Date date = null;
        try {
            SimpleDateFormat fullDateFormat = new SimpleDateFormat(YYYY_MM_DD_HH_MM, Locale.getDefault());
            if (nextClassDate != null && !nextClassDate.isEmpty()) {
                try {
                    date = fullDateFormat.parse(nextClassDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            return "";
        }
        if (date != null) {
            return new SimpleDateFormat(EEE_HH_MM).format(date);
        }
        return "";
    }
}
