package com.example.quwi.net.model.request;

import com.google.gson.annotations.SerializedName;

public class RegistrationBody {
    @SerializedName("email")
    private String mEmail;
    @SerializedName("name")
    private String mName;
    @SerializedName("password")
    private String mPassword;

    public RegistrationBody(String mEmail, String mName, String mPassword) {
        this.mEmail = mEmail;
        this.mName = mName;
        this.mPassword = mPassword;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }
}
