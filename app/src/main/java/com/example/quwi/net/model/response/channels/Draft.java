
package com.example.quwi.net.model.response.channels;

import com.google.gson.annotations.SerializedName;

public class Draft {

    @SerializedName("id_reply")
    private Object mIdReply;
    @SerializedName("text")
    private Object mText;

    public Object getIdReply() {
        return mIdReply;
    }

    public void setIdReply(Object idReply) {
        mIdReply = idReply;
    }

    public Object getText() {
        return mText;
    }

    public void setText(Object text) {
        mText = text;
    }

}
