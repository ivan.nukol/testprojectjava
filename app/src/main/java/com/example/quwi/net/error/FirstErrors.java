
package com.example.quwi.net.error;

import com.google.gson.annotations.SerializedName;

public class FirstErrors {

    @SerializedName("email")
    private String mEmail;

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

}
