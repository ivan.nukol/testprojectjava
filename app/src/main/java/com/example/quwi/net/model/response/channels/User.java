
package com.example.quwi.net.model.response.channels;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("avatar_url")
    private String mAvatarUrl;
    @SerializedName("name")
    private String mName;

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        mAvatarUrl = avatarUrl;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
