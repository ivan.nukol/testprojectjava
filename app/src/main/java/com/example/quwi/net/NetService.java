package com.example.quwi.net;

import static com.example.quwi.BuildConfig.BASE_URL;

import com.example.quwi.net.api.AuthorizationApi;
import com.example.quwi.net.api.ChannelApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetService {

    private static Retrofit retrofitAuthorization = null;
    private static Retrofit retrofitChannelApi= null;
    private static final int TIME_OUT = 10;

    public static AuthorizationApi getAuthApi() {
        if (retrofitAuthorization == null) {
            retrofitAuthorization = getClient(BASE_URL);
        }
        return retrofitAuthorization.create(AuthorizationApi.class);
    }

    public static ChannelApi getChannelApi() {
        if (retrofitChannelApi == null) {
            retrofitChannelApi = getClient(BASE_URL);
        }
        return retrofitChannelApi.create(ChannelApi.class);
    }

    private static Retrofit getClient(String url) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        Gson gson = new GsonBuilder().setLenient().create();
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build();
        return new Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }
}
