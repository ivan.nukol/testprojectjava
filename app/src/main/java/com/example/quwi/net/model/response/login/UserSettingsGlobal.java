
package com.example.quwi.net.model.response.login;

import com.google.gson.annotations.SerializedName;

public class UserSettingsGlobal {

    @SerializedName("client_settings")
    private Object mClientSettings;
    @SerializedName("is_chat_email_notification")
    private Boolean mIsChatEmailNotification;

    public Object getClientSettings() {
        return mClientSettings;
    }

    public void setClientSettings(Object clientSettings) {
        mClientSettings = clientSettings;
    }

    public Boolean getIsChatEmailNotification() {
        return mIsChatEmailNotification;
    }

    public void setIsChatEmailNotification(Boolean isChatEmailNotification) {
        mIsChatEmailNotification = isChatEmailNotification;
    }

}
