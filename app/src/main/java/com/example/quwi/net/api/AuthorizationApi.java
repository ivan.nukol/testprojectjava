package com.example.quwi.net.api;


import com.example.quwi.net.model.request.LoginBody;
import com.example.quwi.net.model.request.RegistrationBody;
import com.example.quwi.net.model.response.login.LoginResponse;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthorizationApi {
    @POST("auth/login")
    Single<LoginResponse> login(@Body LoginBody loginBody);

    @POST("auth/signup")
    Single<LoginResponse> registration(@Body RegistrationBody registrationBody);
}
