package com.example.quwi.net.api;

import com.example.quwi.net.model.response.channels.Channels;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

public interface ChannelApi {

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @GET("chat-channels")
    Single<Channels> getChannels(@Header("Authorization") String token);
}
