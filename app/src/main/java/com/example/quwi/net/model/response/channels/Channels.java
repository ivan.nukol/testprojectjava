
package com.example.quwi.net.model.response.channels;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Channels {

    @SerializedName("channels")
    private List<Channel> mChannels;

    public List<Channel> getChannels() {
        return mChannels;
    }

    public void setChannels(List<Channel> channels) {
        mChannels = channels;
    }

}
