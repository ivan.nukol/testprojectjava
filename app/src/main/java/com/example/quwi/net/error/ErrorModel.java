package com.example.quwi.net.error;

import com.google.gson.annotations.SerializedName;

public class ErrorModel {
    @SerializedName("first_errors")
    private FirstErrors mFirstErrors;

    public FirstErrors getFirstErrors() {
        return mFirstErrors;
    }

    public void setFirstErrors(FirstErrors firstErrors) {
        mFirstErrors = firstErrors;
    }
}
