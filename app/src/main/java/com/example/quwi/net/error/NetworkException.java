package com.example.quwi.net.error;

import androidx.annotation.Nullable;

public class NetworkException extends Exception {
    private ErrorModel error;

    public NetworkException(ErrorModel error, Throwable cause) {
        super(cause);
        this.error = error;
    }
    public String message() {
        return error.getFirstErrors().getEmail();
    }

    @Nullable
    @Override
    public String getMessage() {
        return error.getFirstErrors().getEmail();
    }
}