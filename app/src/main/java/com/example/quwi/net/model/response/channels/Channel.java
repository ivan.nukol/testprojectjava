
package com.example.quwi.net.model.response.channels;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Channel {
    @SerializedName("draft")
    private Draft mDraft;
    @SerializedName("dta_change_msg")
    private String mDtaChangeMsg;
    @SerializedName("dta_create")
    private String mDtaCreate;
    @SerializedName("dta_last_read")
    private String mDtaLastRead;
    @SerializedName("id")
    private Long mId;
    @SerializedName("id_partner")
    private Long mIdPartner;
    @SerializedName("id_users")
    private List<Long> mIdUsers;
    @SerializedName("is_hide_in_chats_list")
    private Boolean mIsHideInChatsList;
    @SerializedName("is_mute")
    private Boolean mIsMute;
    @SerializedName("is_starred")
    private Boolean mIsStarred;
    @SerializedName("is_unread_manual")
    private Boolean mIsUnreadManual;
    @SerializedName("message_last")
    private MessageLast mMessageLast;
    @SerializedName("mute_until_period")
    private Long mMuteUntilPeriod;
    @SerializedName("pin_to_top")
    private Boolean mPinToTop;
    @SerializedName("type")
    private String mType;

    public Channel(Long mId, Draft mDraft, String mDtaChangeMsg, String mDtaCreate, String mDtaLastRead, Long mIdPartner, List<Long> mIdUsers, Boolean mIsHideInChatsList, Boolean mIsMute, Boolean mIsStarred, Boolean mIsUnreadManual, MessageLast mMessageLast, Long mMuteUntilPeriod, Boolean mPinToTop, String mType) {
        this.mDraft = mDraft;
        this.mDtaChangeMsg = mDtaChangeMsg;
        this.mDtaCreate = mDtaCreate;
        this.mDtaLastRead = mDtaLastRead;
        this.mId = mId;
        this.mIdPartner = mIdPartner;
        this.mIdUsers = mIdUsers;
        this.mIsHideInChatsList = mIsHideInChatsList;
        this.mIsMute = mIsMute;
        this.mIsStarred = mIsStarred;
        this.mIsUnreadManual = mIsUnreadManual;
        this.mMessageLast = mMessageLast;
        this.mMuteUntilPeriod = mMuteUntilPeriod;
        this.mPinToTop = mPinToTop;
        this.mType = mType;
    }

    public Draft getDraft() {
        return mDraft;
    }

    public void setDraft(Draft draft) {
        mDraft = draft;
    }

    public String getDtaChangeMsg() {
        return mDtaChangeMsg;
    }

    public void setDtaChangeMsg(String dtaChangeMsg) {
        mDtaChangeMsg = dtaChangeMsg;
    }

    public String getDtaCreate() {
        return mDtaCreate;
    }

    public void setDtaCreate(String dtaCreate) {
        mDtaCreate = dtaCreate;
    }

    public String getDtaLastRead() {
        return mDtaLastRead;
    }

    public void setDtaLastRead(String dtaLastRead) {
        mDtaLastRead = dtaLastRead;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getIdPartner() {
        return mIdPartner;
    }

    public void setIdPartner(Long idPartner) {
        mIdPartner = idPartner;
    }


    public List<Long> getIdUsers() {
        return mIdUsers;
    }

    public void setIdUsers(List<Long> idUsers) {
        mIdUsers = idUsers;
    }

    public Boolean getIsHideInChatsList() {
        return mIsHideInChatsList;
    }

    public void setIsHideInChatsList(Boolean isHideInChatsList) {
        mIsHideInChatsList = isHideInChatsList;
    }

    public Boolean getIsMute() {
        return mIsMute;
    }

    public void setIsMute(Boolean isMute) {
        mIsMute = isMute;
    }

    public Boolean getIsStarred() {
        return mIsStarred;
    }

    public void setIsStarred(Boolean isStarred) {
        mIsStarred = isStarred;
    }

    public Boolean getIsUnreadManual() {
        return mIsUnreadManual;
    }

    public void setIsUnreadManual(Boolean isUnreadManual) {
        mIsUnreadManual = isUnreadManual;
    }

    public MessageLast getMessageLast() {
        return mMessageLast;
    }

    public void setMessageLast(MessageLast messageLast) {
        mMessageLast = messageLast;
    }

    public Long getMuteUntilPeriod() {
        return mMuteUntilPeriod;
    }

    public void setMuteUntilPeriod(Long muteUntilPeriod) {
        mMuteUntilPeriod = muteUntilPeriod;
    }

    public Boolean getPinToTop() {
        return mPinToTop;
    }

    public void setPinToTop(Boolean pinToTop) {
        mPinToTop = pinToTop;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

}
