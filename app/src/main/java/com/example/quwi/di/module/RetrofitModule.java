package com.example.quwi.di.module;

import com.example.quwi.net.NetService;
import com.example.quwi.net.api.AuthorizationApi;
import com.example.quwi.net.api.ChannelApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RetrofitModule {

    @Provides
    @Singleton
    AuthorizationApi getAuthApi() {
        return NetService.getAuthApi();
    }

    @Provides
    @Singleton
    ChannelApi getChannelApi() {
        return NetService.getChannelApi();
    }

}
