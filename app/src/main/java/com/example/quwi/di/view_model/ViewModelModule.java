package com.example.quwi.di.view_model;


import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.quwi.ui.authorization.login.LoginVM;
import com.example.quwi.ui.authorization.registration.RegistrationVM;
import com.example.quwi.ui.main.MainVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public interface ViewModelModule {
    @Binds
    ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(LoginVM.class)
    ViewModel bindLoginVM(LoginVM viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MainVM.class)
    ViewModel bindMainVM(MainVM viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RegistrationVM.class)
    ViewModel bindRegistrationVM(RegistrationVM viewModel);
}
