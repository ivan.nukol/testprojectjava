package com.example.quwi.di;

import android.app.Application;

import com.example.quwi.QuwiApp;
import com.example.quwi.di.activity.MainActivityModule;
import com.example.quwi.di.module.AppModule;
import com.example.quwi.di.module.DatabaseModule;
import com.example.quwi.di.module.Repository;
import com.example.quwi.di.module.RetrofitModule;
import com.example.quwi.di.view_model.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = {
        AppModule.class,
        AndroidInjectionModule.class,
        MainActivityModule.class,
        ViewModelModule.class,
        DatabaseModule.class,
        Repository.class,
        RetrofitModule.class,
})
public interface AppComponent extends AndroidInjector<QuwiApp> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    @Override
    void inject(QuwiApp app);

}