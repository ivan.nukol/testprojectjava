package com.example.quwi.di.module;

import android.content.Context;

import androidx.room.Room;

import com.example.quwi.db.AppDatabase;
import com.example.quwi.db.dao.ChannelsDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {
    @Singleton
    @Provides
    AppDatabase createBd(Context applicationContext) {
        return Room.databaseBuilder(
                applicationContext,
                AppDatabase.class, AppDatabase.NAME_BD
        ).fallbackToDestructiveMigration().build();
    }

    @Singleton
    @Provides
    ChannelsDao provideChannelsDao(AppDatabase appDatabase ) {
        return appDatabase.getChannelsDao();
    }
}
