package com.example.quwi.di.fragment;


import com.example.quwi.ui.authorization.login.LoginFragment;
import com.example.quwi.ui.authorization.registration.RegistrationFragment;
import com.example.quwi.ui.main.MainFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract LoginFragment injectLoginFragment();

    @ContributesAndroidInjector
    abstract MainFragment injectMainFragment();

    @ContributesAndroidInjector
    abstract RegistrationFragment injectRegistrationFragment();
}
