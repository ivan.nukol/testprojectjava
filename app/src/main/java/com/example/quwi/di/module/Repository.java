package com.example.quwi.di.module;

import com.example.quwi.repository.authorization.AuthorizationRepository;
import com.example.quwi.repository.authorization.AuthorizationRepositoryImpl;
import com.example.quwi.repository.channel.ChannelRepository;
import com.example.quwi.repository.channel.ChannelRepositoryImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class Repository {
    @Binds
    abstract AuthorizationRepository getAuthorizationRepository(AuthorizationRepositoryImpl authorizationRepositoryImpl);

    @Binds
    abstract ChannelRepository getChannelRepository(ChannelRepositoryImpl ChannelRepositoryImpl);
}
