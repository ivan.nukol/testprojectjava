package com.example.quwi.di.module;

import android.app.Application;
import android.content.Context;

import com.example.quwi.di.view_model.ViewModelModule;
import com.example.quwi.preference.SharedPreferencesData;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ViewModelModule.class})
public class AppModule {

    @Singleton
    @Provides
    Context provideApplicationContext(Application application) {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    SharedPreferencesData provideSharedPreferencesData(Application application) {
        return new SharedPreferencesData(application);
    }
}
