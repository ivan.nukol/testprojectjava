package com.example.quwi.di.activity;

import com.example.quwi.ui.MainActivity;
import com.example.quwi.di.dialog.DialogBuildersModule;
import com.example.quwi.di.fragment.FragmentBuildersModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = {FragmentBuildersModule.class, DialogBuildersModule.class})
    abstract MainActivity contributeMainActivity();
}