package com.example.quwi.ui.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quwi.R;
import com.example.quwi.databinding.ItemChannelBinding;
import com.example.quwi.net.model.response.channels.Channel;
import com.example.quwi.utils.DateFormat;
import com.example.quwi.utils.GlideUtils;

import java.util.ArrayList;
import java.util.List;

public class ChannelAdapter extends RecyclerView.Adapter<ChannelAdapter.ChannelAdapterHolder> {
    List<Channel> items = new ArrayList<>();

    public void setItems(List<Channel> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ChannelAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemChannelBinding binding = ItemChannelBinding.inflate(inflater, parent, false);
        return new ChannelAdapterHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChannelAdapterHolder holder, int position) {
        Channel item = items.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ChannelAdapterHolder extends RecyclerView.ViewHolder {
        private ItemChannelBinding binding;

        ChannelAdapterHolder(ItemChannelBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Channel item) {
            setupUI(item);
            binding.executePendingBindings();

        }

        private void setupUI(Channel item) {
            if (item.getIdUsers().contains(item.getIdPartner()) && item.getIdUsers().size() == 1) {
                setupSaveMessage(item);
            } else {
                setupOtherPeople(item);
            }
        }

        private void setupOtherPeople(Channel item) {
            if (item.getMessageLast() != null && item.getMessageLast().getUser() != null) {
                GlideUtils.glideSetImgForAvatar(
                        binding.imgAvatar,
                        item.getMessageLast().getUser().getAvatarUrl(),
                        R.drawable.ic_placeholder_avatar
                );

                binding.tvName.setText(item.getMessageLast().getUser().getName());
                binding.tvLastMsg.setText(item.getMessageLast().getText());
                binding.tvTime.setText(DateFormat.getTime(item.getDtaCreate()));
                binding.imgReadIcon.setVisibility(item.getMessageLast().getIsRead() > 0 ? View.VISIBLE : View.GONE);
                binding.imgPinIcon.setVisibility(item.getPinToTop() ? View.VISIBLE : View.GONE);

            }
        }

        private void setupSaveMessage(Channel item) {
            binding.imgAvatar.setImageResource(R.drawable.ic_history);
            binding.tvName.setText(R.string.saved_messages);
            if (item.getMessageLast() != null && item.getMessageLast().getUser() != null) {
                binding.tvLastMsg.setText(item.getMessageLast().getText());
                binding.tvTime.setText(DateFormat.getTime(item.getDtaCreate()));
                binding.imgReadIcon.setVisibility(item.getMessageLast().getIsRead() > 0 ? View.VISIBLE : View.GONE);
                binding.imgPinIcon.setVisibility(item.getPinToTop() ? View.VISIBLE : View.GONE);
            }
        }

    }
}
