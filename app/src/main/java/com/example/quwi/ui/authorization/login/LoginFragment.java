package com.example.quwi.ui.authorization.login;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.quwi.R;
import com.example.quwi.base.BaseFragment;
import com.example.quwi.databinding.FragmentLoginBinding;
import com.example.quwi.net.model.response.login.LoginResponse;
import com.example.quwi.utils.ValidationFieldUtils;
import com.google.android.material.textfield.TextInputLayout;

public class LoginFragment extends BaseFragment<FragmentLoginBinding, LoginVM> {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupListener();
        setupLiveData();
    }

    private void setupListener() {
        binding.btnLogin.setOnClickListener(v -> login());
        binding.tvCreateAccount.setOnClickListener(v -> createAccount());
        binding.etEmail.addTextChangedListener(new ValidationFieldUtils(s -> binding.etEmailLayout.setErrorEnabled(false)));
        binding.etPassword.addTextChangedListener(new ValidationFieldUtils(s -> binding.etPasswordLayout.setErrorEnabled(false)));
    }

    private void login() {
        if (isNotValidData()) return;

        binding.loader.progressCircular.setVisibility(View.VISIBLE);
        viewModel.login(binding.etEmail.getText().toString(), binding.etPassword.getText().toString());
    }

    private Boolean isNotValidData() {
        if (binding.etEmail.getText().toString().isEmpty()) {
            binding.etEmailLayout.setError(getString(R.string.email_cannot_be_empty));
            return true;
        }
        if (binding.etPassword.getText().toString().isEmpty()) {
            binding.etPasswordLayout.setError(getString(R.string.password_cannot_be_empty));
            return true;
        }
        return false;
    }

    private void createAccount() {
        navigationMain(LoginFragmentDirections.actionToRegistrationFragment());
    }

    private void setupLiveData() {
        viewModel.getLoader().observe(getViewLifecycleOwner(), it -> binding.loader.progressCircular.setVisibility(View.GONE));
        viewModel.getLogin().observe(getViewLifecycleOwner(), this::openMainScreen);
    }

    private void openMainScreen(LoginResponse it) {
        binding.loader.progressCircular.setVisibility(View.GONE);
        navigationMain(LoginFragmentDirections.actionToMainFragment());
    }
}