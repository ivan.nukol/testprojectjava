package com.example.quwi.ui.authorization.registration;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.quwi.base.BaseViewModel;
import com.example.quwi.base.MutableEventLiveData;
import com.example.quwi.net.model.request.LoginBody;
import com.example.quwi.net.model.request.RegistrationBody;
import com.example.quwi.net.model.response.login.LoginResponse;
import com.example.quwi.repository.authorization.AuthorizationRepository;

import javax.inject.Inject;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class RegistrationVM extends BaseViewModel {

    private AuthorizationRepository authorizationRepository;

    private final MutableEventLiveData<LoginResponse> login = new MutableEventLiveData<>();

    public LiveData<LoginResponse> getLogin() {
        return login;
    }

    @Inject
    public RegistrationVM(AuthorizationRepository authorizationRepository) {
        this.authorizationRepository = authorizationRepository;
    }

    void registration(String email, String name, String password) {
        authorizationRepository.registration(new RegistrationBody(email, name, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> setLoader(true))
                .subscribe(this::login, super::checkError);
    }

    private void login(LoginResponse it) {
        login.postValue(it);
    }
}
