package com.example.quwi.ui.authorization.registration;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.quwi.R;
import com.example.quwi.base.BaseFragment;
import com.example.quwi.databinding.FragmentRegistrationBinding;
import com.example.quwi.net.model.response.login.LoginResponse;
import com.example.quwi.utils.ValidationFieldUtils;


public class RegistrationFragment extends BaseFragment<FragmentRegistrationBinding, RegistrationVM> {

    private static final int SIZE_PASSWORD_MIN = 6;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupListener();
        setupLiveData();
    }

    private void setupListener() {
        binding.etEmail.addTextChangedListener(new ValidationFieldUtils(s -> binding.etEmailLayout.setErrorEnabled(false)));
        binding.etPassword.addTextChangedListener(new ValidationFieldUtils(s -> binding.etPasswordLayout.setErrorEnabled(false)));
        binding.etName.addTextChangedListener(new ValidationFieldUtils(s -> binding.etNameLayout.setErrorEnabled(false)));
        binding.etPasswordRepeat.addTextChangedListener(new ValidationFieldUtils(s -> binding.etPasswordRepeatLayout.setErrorEnabled(false)));

        binding.backBtn.setOnClickListener(it -> back());
        binding.btnCreateAccount.setOnClickListener(it -> createAccount());
    }


    private void createAccount() {
        if (isNotValidData()) return;
        binding.loader.progressCircular.setVisibility(View.VISIBLE);

        viewModel.registration(binding.etEmail.getText().toString(),
                binding.etName.getText().toString(),
                binding.etPassword.getText().toString());
    }

    private Boolean isNotValidData() {
        if (binding.etName.getText().toString().isEmpty()) {
            binding.etNameLayout.setError(getString(R.string.name_cannot_be_empty));
            return true;
        }
        if (binding.etEmail.getText().toString().isEmpty()) {
            binding.etEmailLayout.setError(getString(R.string.email_cannot_be_empty));
            return true;
        }
        if (binding.etPassword.getText().toString().isEmpty()) {
            binding.etPasswordLayout.setError(getString(R.string.password_cannot_be_empty));
            return true;
        }
        if (binding.etPassword.getText().length() < SIZE_PASSWORD_MIN) {
            binding.etPasswordLayout.setError(getString(R.string.password_must_be_longer_than));
            return true;
        }
        if (binding.etPasswordRepeat.getText().toString().isEmpty()) {
            binding.etPasswordRepeatLayout.setError(getString(R.string.repeat_password_cannot_be_empty));
            return true;
        }

        if (!binding.etPassword.getText().toString().equals(binding.etPasswordRepeat.getText().toString())) {
            binding.etPasswordRepeatLayout.setError(getString(R.string.passwords_must_match));
            return true;
        }
        return false;
    }

    private void setupLiveData() {
        viewModel.getLoader().observe(getViewLifecycleOwner(), it -> binding.loader.progressCircular.setVisibility(View.GONE));
        viewModel.getLogin().observe(getViewLifecycleOwner(), this::openMainScreen);
    }

    private void openMainScreen(LoginResponse it) {
        binding.loader.progressCircular.setVisibility(View.GONE);
        navigationMain(RegistrationFragmentDirections.actionToMainFragment());
    }
}