package com.example.quwi.ui.main;


import androidx.lifecycle.LiveData;

import com.example.quwi.base.BaseViewModel;
import com.example.quwi.base.MutableEventLiveData;
import com.example.quwi.net.model.response.channels.Channel;
import com.example.quwi.preference.PrefEntity;
import com.example.quwi.preference.SharedPreferencesData;
import com.example.quwi.repository.channel.ChannelRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class MainVM extends BaseViewModel {
    private ChannelRepository channelRepository;
    private SharedPreferencesData sharedPreferencesData;
    private final MutableEventLiveData<List<Channel>> channels = new MutableEventLiveData<>();

    @Inject
    public MainVM(ChannelRepository channelRepository, SharedPreferencesData sharedPreferencesData) {
        this.channelRepository = channelRepository;
        this.sharedPreferencesData = sharedPreferencesData;
        getChannels();
        subscribeChannels();
    }

    public LiveData<List<Channel>> getLiveDataChannel() {
        return channels;
    }

    void getChannels() {
        channelRepository.getChannels(sharedPreferencesData.getPreferenceString(PrefEntity.TOKEN))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> setLoader(true))
                .subscribe(it -> {
                }, super::checkError);
    }

    void subscribeChannels() {
        channelRepository.subscribeChannels().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(channels::postValue);
    }

    public void logout() {
        channelRepository.clareChannelDB();
        sharedPreferencesData.clareDate(PrefEntity.TOKEN);
    }
}
