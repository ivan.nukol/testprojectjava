package com.example.quwi.ui.main;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.View;

import com.example.quwi.base.BaseFragment;
import com.example.quwi.databinding.FragmentMainBinding;
import com.example.quwi.net.model.response.channels.Channel;
import com.example.quwi.ui.main.adapter.ChannelAdapter;

import java.util.List;

public class MainFragment extends BaseFragment<FragmentMainBinding, MainVM> {


    private ChannelAdapter channelAdapter = new ChannelAdapter();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUi();
        setupLiveData();
    }

    private void setupUi() {
        binding.loader.progressCircular.setVisibility(View.VISIBLE);
        binding.adapter.setAdapter(channelAdapter);
        binding.swipeRefresh.setOnRefreshListener(() -> {
            viewModel.getChannels();
        });
        binding.btnLogout.setOnClickListener(it -> logout());
    }

    private void logout() {
        viewModel.logout();
        navigationMain(MainFragmentDirections.actionToLoginFragment());
    }

    private void setupLiveData() {
        viewModel.getLiveDataChannel().observe(getViewLifecycleOwner(), this::setAdapter);
        viewModel.getLoader().observe(getViewLifecycleOwner(), this::gideLoader);
    }

    private void gideLoader(Boolean aBoolean) {
        binding.swipeRefresh.setRefreshing(false);
        binding.loader.progressCircular.setVisibility(View.GONE);
    }

    private void setAdapter(List<Channel> channels) {
        channelAdapter.setItems(channels);
        if (channels.size() != 0) {
            binding.swipeRefresh.setRefreshing(false);
            binding.loader.progressCircular.setVisibility(View.GONE);
        }
    }
}