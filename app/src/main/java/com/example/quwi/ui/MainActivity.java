package com.example.quwi.ui;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.example.quwi.R;
import com.example.quwi.base.BaseActivity;

import com.example.quwi.databinding.ActivityMainBinding;
import com.example.quwi.preference.PrefEntity;
import com.example.quwi.preference.SharedPreferencesData;
import com.example.quwi.ui.authorization.login.LoginFragment;
import com.example.quwi.ui.main.MainFragment;


import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements HasAndroidInjector {

    @Inject
    protected DispatchingAndroidInjector<Object> dispatchingAndroidInjector;

    @Inject
    protected SharedPreferencesData sharedPreferencesData;

    private NavController navHost;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
        initNavController();
        openMainScreen();
    }

    private void initViewModel() {
    }

    private void initNavController() {
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
        getSupportFragmentManager().beginTransaction().setPrimaryNavigationFragment(navHostFragment);
        navHost = navHostFragment.getNavController();
    }

    private void openMainScreen() {
        if (!sharedPreferencesData.getPreferenceString(PrefEntity.TOKEN).isEmpty()) {
            navHost.navigate(R.id.mainFragment);
        }
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    protected int getLayoutResID() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        if (!navHost.getCurrentDestination().getLabel().equals(MainFragment.class.getSimpleName())
                && !navHost.getCurrentDestination().getLabel().equals(LoginFragment.class.getSimpleName())) {
            super.onBackPressed();
        }
    }
}